# Kenzan code challenge

CRUD REST API code to manage employee records.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

You can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

Or, you can create the `jar` file using:

```shell
mvn clean install
```

And running the app like:

```shell
java -jar target/kenzan-0.1.0.jar
```

## Test REST API

The easiest way to test the endpoints is to use a client like [Postman](https://www.getpostman.com/).

The root path for this API is /employees, and these are the methods implemented:

* `GET`: Get all ACTIVE employees, `http://localhost:8080/employees`
* `GET`: Get employee by an ID, only if it is ACTIVE. `http://localhost:8080/employees/{ID}`
* `POST`: Create a new employee, by default it is ACTIVE. `http://localhost:8080/employees`
* `PUT`: Update an existing employee, the employee object is updated completly based on the request payload.
* `PATCH`: Update part of an existing employee, only the fields in the request are updated.
* `DELETE`: SOFT Delete, only the status field value is changed to 'INACTIVE'. This method requires 'Basic Authentication' with a user with 'Admin' role.

The application has a default user with 'Admin' role:

user: `admin`
password: `kenzanAdmin`


This an example of the json object that has to be send in the request:

```shell
{
	"firstName": "John",
	"middleInitial": "S",
	"lastName": "Doe",
	"dateOfBirth": 240897748000,
	"dateOfEmployment": 240897748000
}
```


