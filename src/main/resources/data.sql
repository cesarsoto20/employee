INSERT INTO employee(ID,first_name, middle_initial, last_name, date_of_birth, date_of_employment, status) values (next value for HIBERNATE_SEQUENCE,'John','S','Doe',PARSEDATETIME('1977-05-13','yyyy-MM-dd'),PARSEDATETIME('2018-01-13','yyyy-MM-dd'),'ACTIVE');
INSERT INTO employee(ID,first_name, middle_initial, last_name, date_of_birth, date_of_employment, status) values (next value for HIBERNATE_SEQUENCE,'Juan','A','Perez',PARSEDATETIME('1980-03-01','yyyy-MM-dd'),PARSEDATETIME('2017-08-20','yyyy-MM-dd'),'ACTIVE');

