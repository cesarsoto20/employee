package com.cz4r.kenzan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KenzanApplication {

	public static void main(String[] args) {
		SpringApplication.run(KenzanApplication.class, args);
	}

}

