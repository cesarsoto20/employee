package com.cz4r.kenzan;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;


public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long>{

    @Query("select e from #{#entityName} e where e.id =?1 and e.status = 'ACTIVE'")
    Employee findOne(Long id);

    default boolean exists(Long id){
        return findOne(id) != null;
    }

    @Override
    @Query("select e from #{#entityName} e where e.status = 'ACTIVE'")
    Iterable<Employee> findAll();

    @Override
    @Query("select count(e) from #{#entityName} e where e.status = 'ACTIVE'")
    long count();

    @Transactional
    @Modifying
    @Query("update #{#entityName} e set e.status='INACTIVE' where e.id=?1 ")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void delete(Long id);

    @Override
    @Transactional
    @Modifying
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    default void delete(Employee e){
        delete(e.getID());
    }

    @Transactional
    @Modifying
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    default void delete(Iterable<? extends Employee> iterable){
        iterable.forEach(entity -> delete(entity.getID()));
    }

    @Override
    @Transactional
    @Modifying
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Query("update #{#entityName} e set e.status='INACTIVE'")
    void deleteAll();

}
